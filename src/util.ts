import cors from "cors";
import e from "express";

export type HandlerFn = (req: e.Request, resp: e.Response) => void;
export type AsyncHandlerFn = (req: e.Request, resp: e.Response) => Promise<any>;

/**
 * Wraps an async function as an express request handler.
 *
 * @param asyncHandler Async handler function that should be bound to this function,
 */
export function wrapAsync(asyncHandler: AsyncHandlerFn) {
  return (req: e.Request, res: e.Response) => {
    if (req.query && req.query.uptimeTick) {
      res.status(200).send();
      return;
    }

    asyncHandler(req, res).then(
      (body) => {
        res.json(body);
      },
      async (err: Error) => {
        res.status(500).send({
          ...err,
          name: err.name,
          code: (err as any).code,
          message: err.message,
          stack: err.stack,
        });
        return;
      }
    );
  };
}

export const corsMiddleware: e.RequestHandler = cors({
  origin: true,
  credentials: true,
});
