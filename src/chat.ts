import e from "express";
import * as k8s from "@kubernetes/client-node";
import * as uuid from "uuid";
import { corsMiddleware, wrapAsync } from "./util";

const kc = new k8s.KubeConfig();
kc.loadFromDefault();

const k8sApi = kc.makeApiClient(k8s.CoreV1Api);

export async function getRooms() {
  console.log("getRooms");
  const { body } = await k8sApi
    .listNamespace(null, null, null, null, "type=chat-room");
  return body.items.map((ns) => ns.metadata.name)
}

export async function createRoom(name: string) {
  return await k8sApi.createNamespace({
    metadata: {
      name,
      labels: {
        type: "chat-room",
      },
    },
  });
}

export async function writeMessage(
  username: string,
  room: string,
  message: string
) {
  return await k8sApi.createNamespacedConfigMap(room, {
    metadata: {
      name: uuid.v4(),
      labels: {
        type: "message",
      },
    },
    data: {
      username,
      message,
    },
  });
}

export async function listMessagesForRoom(room: string) {
  const { body } = await k8sApi.listNamespacedConfigMap(
    room,
    null,
    null,
    null,
    null,
    "type=message"
  );

  return body.items.map((cfg) => ({
    ...cfg.data,
    timestamp: cfg.metadata.creationTimestamp,
  }));
}

const chat = e();
chat.use(corsMiddleware);

chat.get("/rooms", wrapAsync(getRooms));
chat.post("/rooms", wrapAsync(async (req) => createRoom(req.body.room)));
chat.get("/rooms/:room/messages", wrapAsync(async (req) =>
  listMessagesForRoom(req.params["room"] as string)
));
chat.post("/rooms/:room/messages", wrapAsync(async (req) =>
  writeMessage(
    req.body["username"] as string,
    req.params["room"] as string,
    req.body["message"] as string
  )
));

export default chat;
