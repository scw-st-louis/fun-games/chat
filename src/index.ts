import { json } from "body-parser";
import express from "express";
import chat from "./chat";

const app = express();

app.use(json());
const port = process.env.PORT || 8080;
app.use("/", chat);
app.listen(port, () => {
  console.log(`Listening on :${port}`);
});

process.on('SIGINT', function() {
  process.exit();
});