FROM node:14-alpine
RUN yarn global add typescript
ADD package.json package.json
ADD yarn.lock yarn.lock
RUN yarn install && yarn cache clean && rm -rf node_modules && yarn build && yarn install --production && yarn cache clean

ADD . app
WORKDIR app

CMD yarn start

ENV PORT 8080
EXPOSE 8080